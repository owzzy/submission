
#include "core.h"
#include <math.h>
#include "cMesh.h"
#include "cCamera.h"

#include "glm/glm.hpp"  
#include "glm/gtc/matrix_transform.hpp" 
#include "glm/gtc/type_ptr.hpp"

#include <iostream>

using namespace std;
using namespace glm;


/////////////////////////////////////////////////////////////////////////////////////
// global variables
/////////////////////////////////////////////////////////////////////////////////////
cScene Scene;

unsigned int initWidth = 1600;
unsigned int initHeight = 1200;

/////////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////////
void resize(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);		// Draw into entire window
}

/////////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////////
void keyboard(GLFWwindow* window, int key, int scancode, int action, int mods) 
{
	// toggles wireframe shader by pressing and holding the SPACEBAR
	if (GLFW_KEY_SPACE)
	{
		Scene.m_Mesh[1].m_shdr = 3;
	}


	// ignore key up (key release) events
	if (action == GLFW_RELEASE) 
		return;	

	cCamera* pCamera = &Scene.m_Camera[Scene.m_cameraId];

	// process keyboard inputs here..
	switch (key)
	{
		case GLFW_KEY_ESCAPE:
		case GLFW_KEY_X:
			glfwSetWindowShouldClose(window, true);
		break;


		// very simple camera controls to help navigate the scene
		case GLFW_KEY_D:
		{
			// rotate camera about x-axis
			pCamera->m_rot.y -= 2.0f;
		}break;

		case GLFW_KEY_A:
		{
			// rotate camera about x-axis
			pCamera->m_rot.y += 2.0f;
		}break;

		case GLFW_KEY_LEFT:
		{
			// move camera about y-axis
			pCamera->m_pos.x -= 0.25f;
			Scene.m_Mesh[4].m_transform.m_pos.x -= 0.25;
		}break;

		case GLFW_KEY_RIGHT:
		{
			// move camera about y-axis
			pCamera->m_pos.x += 0.25f;
			Scene.m_Mesh[4].m_transform.m_pos.x += 0.25;
		}break;

		case GLFW_KEY_UP:
		{
			pCamera->m_pos.z -= 0.25f;
			Scene.m_Mesh[4].m_transform.m_pos.z -= 0.25;
		}break;

		case GLFW_KEY_DOWN:
		{
			pCamera->m_pos.z += 0.25f;
			Scene.m_Mesh[4].m_transform.m_pos.z += 0.25;
		}break;

		default:
		{
		}
	}

	if (GLFW_KEY_SPACE)
	{
		Scene.m_Mesh[1].m_shdr = 4;
	}
}

/////////////////////////////////////////////////////////////////////////////////////
// renderScene
/////////////////////////////////////////////////////////////////////////////////////
void renderScene()
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(true);

	// Clear the rendering window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	
	////////////////////////////////////////////////////////////////////////////////
	// Render mesh objects here..
	////////////////////////////////////////////////////////////////////////////////
	Scene.render();

	glBindVertexArray(0);       	
}

/////////////////////////////////////////////////////////////////////////////////////
// printDebugInfo() - print version info to console
/////////////////////////////////////////////////////////////////////////////////////
void printDebugInfo()
{
	// Print info of GPU and supported OpenGL version
	printf("Renderer: %s\n", glGetString(GL_RENDERER));
	printf("OpenGL version supported %s\n", glGetString(GL_VERSION));

#ifdef GL_SHADING_LANGUAGE_VERSION
	printf("Supported GLSL version is %s.\n", (char *)glGetString(GL_SHADING_LANGUAGE_VERSION));
#endif

	printf("Using GLEW version %s.\n", glewGetString(GLEW_VERSION));
	printf("------------------------------\n");
	printf("Press ESCAPE or 'X' or 'x' to exit.\n");
}

/////////////////////////////////////////////////////////////////////////////////////
// DebugOutput
// Used to output openGL debug info
// KHR_debug runs on all systems running openGL4.3 or later
/////////////////////////////////////////////////////////////////////////////////////
void DebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* msg, const void* data)
{
	cout << "debug call: " << msg << endl;
}

/////////////////////////////////////////////////////////////////////////////////////
// main()
/////////////////////////////////////////////////////////////////////////////////////
int main() 
{
	// initialise glfw
	glfwInit();

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

	GLFWwindow* window = glfwCreateWindow(initWidth, initHeight, "GraphicsTemplate", NULL, NULL);
	if (window == NULL)
	{
		fprintf(stdout, "Failed to create GLFW window!\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	
	
	// initialise glew
	glewInit();

	// print version info 
	printDebugInfo();

	glDebugMessageCallback(DebugOutput, NULL);

	// Set callback function for resizing the window
	glfwSetFramebufferSizeCallback(window, resize);

	// Set callback for keyboard events
	glfwSetKeyCallback(window, keyboard);

	resize(window, initWidth, initHeight);

	////////////////////////////////////////////////////
	// initialise scene - geometry and shaders etc
	////////////////////////////////////////////////////
	Scene.init();

	// Loop while program is not terminated.
	while (!glfwWindowShouldClose(window)) {

		renderScene();						// Render into the current buffer
		glfwSwapBuffers(window);			// Displays what was just rendered (using double buffering).

		// Poll events (key presses, mouse events)
//		glfwWaitEvents();					// Use this if no animation.
		//glfwWaitEventsTimeout(1.0/60.0);	// Use this to animate at 60 frames/sec (timing is NOT reliable)
		glfwPollEvents();				// Use this version when animating as fast as possible
	}

	glfwTerminate();
	return 0;
}

